
### Big Question: When is a town or village too small?

  1. Measure smallness

  2. Replace locality with wildlife

    1. Adjustments for local flora/fauna?

    2. Density?

    3. Timeframe for conversion?
